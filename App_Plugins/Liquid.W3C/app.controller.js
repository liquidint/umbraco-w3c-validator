﻿angular
    .module('umbraco')
    .controller('liquid.w3c.validator', function ($scope, $location, $http, editorState) {

        $scope.iframeLoaded = function () {
            $('#w3c-iframe').css('background', 'none');
        };

        $scope.runW3C = function () {
            var pageUrl = '';
            var hostname = $location.host(); //Host Name (e.g. example.com)
            $scope.currentUrls = [];
            updateCurrentUrls();
            var currentVariant = _.find(editorState.current.variants, function (v) {
                return v.active;
            });

            if ($scope.currentUrls === null) {
                document.getElementById('w3cError').innerHTML = "<strong>This page does not have a URL so results could not be rendered.</strong><br />";
            }
            else
            {
                //If localhost, find url that has http in it since it can't validate on localhost. If not localhost find url path and append to domain.
                if (hostname === 'localhost') {
                    let i;
                    for (i = 0; i < $scope.currentUrls.length; i++) {
                        if ($scope.currentUrls[i].isUrl) {
                            let url = $scope.currentUrls[i].text;
                            if (url.indexOf('http') > -1) {
                                pageUrl = $scope.currentUrls[i].text;
                                break;
                            }
                        }
                    }
                } else {
                    let i;
                    for (i = 0; i < $scope.currentUrls.length; i++) {
                        if ($scope.currentUrls[i].isUrl) {
                            let url = $scope.currentUrls[i].text;
                            if (url.indexOf('http') === -1) {
                                pageUrl = $location.protocol() + '://' + hostname + $scope.currentUrls[i].text;
                                break;
                            }
                        }
                    }
                }

                document.getElementById('w3cError').innerHTML = "<strong>Showing results for: </strong> " + pageUrl + "<br />";
                var url = 'https://validator.w3.org/nu/?doc=' + pageUrl;
                $http.get(url)
                    .then(function (response) {

                        let doc = document.getElementById('w3c-iframe').contentWindow.document;
                        doc.open();
                        doc.write("<div id='results'>" + $(response.data).filter('#results').html()) + "</div>";
                        doc.close();

                        //add style sheet
                        let link = document.createElement("link");
                        link.href = "/App_Plugins/Liquid.W3C/w3cstyle.css";
                        link.rel = "stylesheet";
                        link.type = "text/css";
                        frames[0].document.head.appendChild(link);

                        //override style & open links in new tab
                        let head = $("#w3c-iframe").contents().find("head");
                        let css = '<style>#results {margin-top: 0 !important; padding-left: 0 !important;}</style>';
                        let base = '<base target="_blank">';
                        $(head).append(base + css);

                    })
                    .catch(function (data) {
                        document.getElementById('w3cError').innerHTML = "<strong>There was an error:</strong><br />" + JSON.stringify(data);
                    });
            }
        };

        function updateCurrentUrls() {
            // never show urls for element types (if they happen to have been created in the content tree)
            if (editorState.current.isElement) {
                $scope.currentUrls = null;
                return;
            }
            // find the urls for the currently selected language
            //TODO need to test with multi language site
            if (editorState.current.variants.length > 1) {
                // nodes with variants
                $scope.currentUrls = _.filter(editorState.current.urls, function (url) {
                    return currentVariant.language.culture === url.culture;
                });
            } else {
                // invariant nodes
                $scope.currentUrls = editorState.current.urls;
            }
        };

        //Can show numbers on badge
        //$scope.model.badge = {
        //    count: 3,
        //    type: 'default' //'warning' // or 'alert' 
        //};
    });
